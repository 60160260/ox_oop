import java.util.Scanner;

public class Game {
	private Player X;
	private Player O;
	private Board board;

	Game() {
		O = new Player('O');
		X = new Player('X');
		board = new Board(X, O);
	}

	void play() {
		showWelcome();
		while (true) {
			while (true) {
				showTable();
				if (board.isFinish()) {
					showResult();
					showStat();
					break;
				}
				showTurn();
				input();
			}
			showBye();
		}
	}

	void showWelcome() {
		board.getWelcome();
	}

	void showTable() {
		char[][] table = board.getTable();
		System.out.println("  1 2 3");
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1 + "|");
			for (int j = 0; j < 3; j++) {
				if (table[i][j] != '\u0000') {
					System.out.print(table[i][j]);
				} else {
					System.out.print(" ");
				}
				System.out.print("|");
			}
			System.out.println();
		}
	}

	void showTurn() {
		System.out.println("Turn " + board.getCurrentPlayer().getName());
	}

	void input() {
		Scanner kb = new Scanner(System.in);
		System.out.print("Plz choose position (R,C) :");
		String type_row = kb.next();
		String type_column = kb.next();
		try {
			int row = Integer.parseInt(type_row) - 1, column = Integer.parseInt(type_column) - 1;
			if (board.setTable(row, column))
				;
			else {
				if (!(row >= 0 && row < 3 && column >= 0 && column < 3)) {
					System.out.println("Row and Column must be number 1 - 3");
				} else {
					System.out.println("Row " + row + " and Column " + column + " can't choose again");
				}
			}
		} catch (Exception e) {
			System.out.println("Row and Column must be number");
		}

	}

	void showResult() {
		board.getWinner();
		board.clearTable();
	}

	void showBye() {
		System.out.println("Thank you for playing game");
	}

	void showStat() {
		System.out.println("Stat X : WIN / DRAW / LOSE : " + X.getWin() + " / " + X.getDraw() + " / " + X.getLose());
		System.out.println("Stat O : WIN / DRAW / LOSE : " + O.getWin() + " / " + O.getDraw() + " / " + O.getLose());
	}
}
